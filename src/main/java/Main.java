public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployee(new JavaDeveloper("John"));
        company.addEmployee(new JavascriptDeveloper("Mark"));
        company.addEmployee(new FullstackDeveloper("Peter"));
        company.startWork();
    }
}
