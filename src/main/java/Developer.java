public interface Developer extends Person {
    void develop();
}
