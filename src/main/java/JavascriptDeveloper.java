public class JavascriptDeveloper extends Worker implements FrontendDeveloper {
    public JavascriptDeveloper(String name) {
        super(name);
    }

    public void develop() {
        writeFront();
    }

    public void work() {
        develop();
    }

    public void writeFront() {
        System.out.println("My name is " + getName() + ". I`m writing code in JavaScript");
    }

}
