public class JavaDeveloper extends Worker implements BackendDeveloper {

    public JavaDeveloper(String name) {
        super(name);
    }


    public void writeBackEnd() {
        System.out.println("My name is " + getName() + ". I`m writing code in Java");
    }

    public void develop() {
        writeBackEnd();
    }

    public void work() {
        develop();
    }
}
