public interface BackendDeveloper extends Developer {

    void writeBackEnd();

}
